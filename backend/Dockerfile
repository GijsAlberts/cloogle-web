# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

FROM cleanlang/nitrile:0.3 AS builder

COPY backend /usr/src/backend
COPY util /usr/src/util
COPY nitrile.yml /usr/src/nitrile.yml
COPY extra_package_config.json /usr/src/extra_package_config.json
WORKDIR /usr/src
RUN cd backend; ./build.sh

FROM debian:bullseye-slim AS runner

WORKDIR /usr/src/backend
RUN apt-get update -qq &&\
	apt-get install -qq libc6-i386 &&\
	rm -rf /var/lib/apt/lists
COPY --from=builder /usr/src/backend/CloogleServer /usr/src/backend/CloogleServer
COPY --from=builder /usr/src/backend/db.jsonl /usr/src/backend/db.jsonl
COPY --from=builder /usr/src/backend/rank_settings.json /usr/src/backend/rank_settings.json
COPY --from=builder /usr/src/backend/typetree.dot /usr/src/backend/typetree.dot
COPY backend/serve /usr/src/backend/serve

EXPOSE 31215

ENTRYPOINT "./serve"
CMD []

definition module Util.SimpleTCPServer

/**
 * Utility module for a simple TCP server.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from StdOverloaded import class zero, class fromString, class toString
from TCPIP import :: IPAddress, :: Port

:: LogMessage req res sentinfo
	= Connected IPAddress
	| Received req
	| Sent res sentinfo
	| Disconnected

:: Logger req res logst sentinfo
	:== (LogMessage req res sentinfo) (?logst) *World -> *(?logst, *World)

:: Server req res st logst sentinfo =
	{ handler           :: !req -> .(st -> *(*World -> *(res, sentinfo, st, *World)))
	, logger            :: ! ?(Logger req res logst sentinfo)
	, port              :: !Int
	, connect_timeout   :: ! ?Int
	, keepalive_timeout :: ! ?Int
	}

serve :: !(Server req res .st logst sentinfo) .st !*World -> *World | fromString req & toString res

# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# cloogle-web. If not, see <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

version: '3.4'
services:
  backend:
    build:
      context: .
      dockerfile: backend/Dockerfile
      target: runner
    ports:
      - "${CLOOGLE_BACKEND_PORT}:31215"
    volumes:
      - "${CLOOGLE_LOG}:/usr/src/cloogle/cloogle.log"
      - "${CLOOGLE_CACHE}:/usr/src/cloogle/cache"
    restart: always
    cap_add:
      - IPC_LOCK

  frontend:
    build:
      context: .
      dockerfile: frontend/Dockerfile
    environment:
      - "CLOOGLE_BACKEND_HOSTNAME=${CLOOGLE_BACKEND_HOSTNAME}"
      - "CLOOGLE_DB_HOST=${CLOOGLE_DB_HOST}"
      - "CLOOGLE_DB_USER=${CLOOGLE_DB_USER}"
      - "CLOOGLE_DB_PASS=${CLOOGLE_DB_PASS}"
      - "CLOOGLE_DB_NAME=${CLOOGLE_DB_NAME}"
    ports:
      - "${CLOOGLE_FRONTEND_PORT}:80"
    volumes:
      - "./frontend/:/var/www/html/"
    restart: always

  db:
    profiles: [with-statistics]
    image: mariadb:latest
    volumes:
      - "./db/:/docker-entrypoint-initdb.d/"
      - "./db/storage/:/var/lib/mysql/"
    environment:
        MYSQL_ROOT_PASSWORD: cloogle
        MYSQL_DATABASE: cloogledb
        MYSQL_USER: cloogle
        MYSQL_PASSWORD: cloogle
    restart: always

  gc:
    profiles: [with-garbage-collection]
    build: gc
    volumes:
      - "./cache:/var/cache"
    restart: always

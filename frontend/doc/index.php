<!DOCTYPE html>
<!--
 - Copyright 2016-2022 the authors (see README.md).
 -
 - This file is part of cloogle-web.
 -
 - Cloogle-web is free software: you can redistribute it and/or modify it under
 - the terms of the GNU Affero General Public License as published by the Free
 - Software Foundation, version 3 of the License.
 -
 - Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 - ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 - FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 - for more details.
 -
 - You should have received a copy of the GNU Affero General Public License
 - along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 -
 - The software is licensed under additional terms under section 7 of the GNU
 - Affero General Public License; see the LICENSE file for details.
-->
<html lang="en">
<head>
	<title>Documentation browser</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="Cloogle is a search engine for the Clean programming language"/>
	<meta name="keywords" content="Clean,Clean language,Concurrent Clean,search,functions,search engine,programming language,clean platform,iTasks,cloogle,hoogle"/>
	<link rel="stylesheet" href="../common.css" type="text/css"/>
	<style>
		td, th {
			padding-right: unset;
		}

		#sidebar {
			min-width: 300px;
		}

		#sidebar h3 {
			background: none;
			font-family: sans-serif;
			font-size: 1.17em;
			font-weight: bold;
			line-height: 22px;
			margin-bottom: 17.55px;
			margin-left: 0;
		}

		#sidebar table a {
			color: inherit !important;
			text-decoration: none;
		}

		#sidebar td {
			white-space: nowrap;
		}

		#viewer {
			padding: 10px;
		}

		#viewer a:visited {
			color: blue !important;
		}

		#viewer > div:first-child {
			position: relative;
		}
	</style>
</head>
<body class="framelike">
	<div id="sidebar">
		<a href="/"><img id="logo" src="../logo.png" alt="Cloogle logo"/></a>
		<h3>Documentation browser</h3>
		<hr/>
		<?php include_once('contents.php'); ?>
		<p><a class="muted" href="/?legal">Terms &amp; Privacy</a></p>
	</div><div id="viewer">
		<?php include_once('src.php'); ?>
		<p>&nbsp;</p>
	</div>
</body>
</html>

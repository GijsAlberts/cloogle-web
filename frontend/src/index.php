<!DOCTYPE html>
<!--
 - Copyright 2016-2022 the authors (see README.md).
 -
 - This file is part of cloogle-web.
 -
 - Cloogle-web is free software: you can redistribute it and/or modify it under
 - the terms of the GNU Affero General Public License as published by the Free
 - Software Foundation, version 3 of the License.
 -
 - Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 - ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 - FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 - for more details.
 -
 - You should have received a copy of the GNU Affero General Public License
 - along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 -
 - The software is licensed under additional terms under section 7 of the GNU
 - Affero General Public License; see the LICENSE file for details.
-->
<html lang="en">
<head>
	<title>Library browser</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="Cloogle is a search engine for the Clean programming language"/>
	<meta name="keywords" content="Clean,Clean language,Concurrent Clean,search,functions,search engine,programming language,clean platform,iTasks,cloogle,hoogle"/>
	<script src="../common.js" defer="defer"></script>
	<script src="../clean-highlighter/clean.js" defer="defer"></script>
	<script src="../browser.js" defer="defer"></script>
	<script src="view.js" defer="defer"></script>
	<link rel="stylesheet" href="../common.css" type="text/css"/>
	<link rel="stylesheet" href="../clean-highlighter/clean.css" type="text/css"/>
	<link rel="stylesheet" href="view.css" type="text/css"/>
</head>
<body class="framelike">
	<div id="sidebar">
		<a href="/"><img id="logo" src="../logo.png" alt="Cloogle logo"/></a>
		<h3>Library browser</h3>
		<label for="icl"><input id="icl" type="checkbox"/> Show implementation</label><br/>
		<div id="tools">
			<input type="button" value="Repo" onclick="repoButtonClick('Repository','blob')"/>
			<input type="button" value="Blame" onclick="repoButtonClick('Blame','blame')"/>
		</div>
		<hr/>
		<?php include_once('lib.php'); ?>
		<br/>
		<p><a class="muted" href="/?legal">Terms &amp; Privacy</a></p>
	</div><div id="viewer">
		<?php include_once('src.php'); ?>
	</div>
</body>
</html>

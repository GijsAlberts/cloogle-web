<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

require_once('./conf.php');

$uacase = 'CASE';
foreach ($user_agents as $name => $ua)
	$uacase .= " WHEN `useragent` LIKE '" . $ua['pattern'] . "' THEN '" . $name . "'";
$uacase .= " ELSE 'Other' END";

$sql =
	"SELECT type, sum(cnt) as sumcnt FROM (
		select
			useragent,
			(SELECT count(*) FROM log
				WHERE useragent_id=useragent.id AND
				" . SQL_NOT_SILLYUSER . " AND
				`date` BETWEEN timestamp('$startTime') AND timestamp('$endTime')) as cnt,
			($uacase) as type
		FROM useragent
		ORDER BY cnt DESC
	) counts
	GROUP BY type
	ORDER BY sumcnt DESC";

$stmt = $db->stmt_init();
if (!$stmt->prepare($sql))
	var_dump($stmt->error);
$stmt->execute();
$stmt->bind_result($type, $count);

$results = [];
while ($stmt->fetch())
	$results[] = ['name' => $type, 'y' => (int) $count];

$stmt->close();

header('Content-Type: text/javascript');
echo "$callback(" . json_encode($results) . ");";

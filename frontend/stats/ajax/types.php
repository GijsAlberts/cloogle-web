<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

require_once('./conf.php');

$sql =
	"SELECT
		count(*),
		sum(case when `query` LIKE '%::%' then 1 else null end),
		sum(case when `query` LIKE 'type %' then 1 else null end),
		sum(case when `query` LIKE 'class %' OR `query` LIKE 'instance %' then 1 else null end),
		sum(case when `query` LIKE 'using %' then 1 else null end)
	FROM `log`
	WHERE
		" . SQL_NOT_SILLYUSER . " AND
		`date` BETWEEN timestamp('$startTime') AND timestamp('$endTime')";

$stmt = $db->stmt_init();
if (!$stmt->prepare($sql))
	var_dump($stmt->error);
$stmt->execute();
$stmt->bind_result($total, $unify, $type, $class, $using);
$stmt->fetch();
$stmt->close();

$results = [
	[ 'name' => 'Unification', 'y' => (int) $unify ],
	[ 'name' => 'Type definition', 'y' => (int) $type ],
	[ 'name' => 'Class', 'y' => (int) $class ],
	[ 'name' => 'Using', 'y' => (int) $using ],
	[ 'name' => 'Name only', 'y' => $total - $unify - $type - $class - $using ]
];

header('Content-Type: text/javascript');
echo "$callback(" . json_encode($results) . ");";

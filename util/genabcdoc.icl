module genabcdoc

/**
 * Main module to generate HTML documentation of ABC instructions.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdFunctions
import StdList
import StdOrdList
import StdString

import Clean.Doc.Markup
import Data.Either
from Data.Func import on, `on`
import Data.List
import Text
import Text.HTML

import Cloogle.API
import Cloogle.DB
import Builtin.ABC

Start = toString (documentation builtin_abc_instructions)

documentation :: ![Either String ABCInstructionEntry] -> HtmlTag
documentation instrs = HtmlTag []
	[ HeadTag []
		[ TitleTag [] [Text "ABC documentation"]
		, StyleTag [TypeAttr "text/css"] [Text ".instruction{border:1px solid black;margin:.5em;padding:.5em;}"]
		]
	, BodyTag []
		[ H1Tag [] [Text "ABC documentation"]
		: concatMap entry instrs ++
		  [H2Tag [] [Text "Index"], index instrs]
		]
	]

index :: ![Either String ABCInstructionEntry] -> HtmlTag
index instrs = UlTag []
	[LiTag [] [ATag [HrefAttr ("#"+++i.aie_instruction)] [Text i.aie_instruction]] \\ i <- sorted]
where
	sorted = sortBy ((<) `on` \e->e.aie_instruction) [i \\ Right i <- instrs]

entry :: !(Either String ABCInstructionEntry) -> [HtmlTag]
entry (Left title) = [H2Tag [] [Text title]]
entry (Right aie)
| aie.aie_description=="There is no documentation for this ABC instruction yet."
	= [CodeTag [] [Text aie.aie_instruction], Text " "]
	= [DivTag [IdAttr aie.aie_instruction, ClassAttr "instruction"]
		[ CodeTag [] (intersperse (Text " ") [Text aie.aie_instruction:[EmTag [] [Text (toString arg)] \\ arg <- aie.aie_arguments]])
		, DivTag [] (documentationToHTML link aie.aie_description)
		]]
where
	link :: !String -> HtmlTag
	link s = ATag [HrefAttr ("#"+++s)] [Text s]
